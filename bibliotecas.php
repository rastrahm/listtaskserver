<?php
// Manejador de la base de datos
include_once 'modules/bd.php';
include_once 'modules/jwt.php';
// Modelos de los datos
include_once 'model/user.php';
include_once 'model/menu.php';
include_once 'model/security.php';
include_once 'model/objects.php';
include_once 'vendor/class.phpmailer.php';
include_once 'vendor/class.smtp.php';
include_once 'vendor/class.pop3.php';
?>
