--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15 (Ubuntu 10.15-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-1.pgdg18.04+1)

-- Started on 2020-12-25 11:04:21 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16408)
-- Name: program; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA program;


ALTER SCHEMA program OWNER TO postgres;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3192 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 281 (class 1255 OID 16599)
-- Name: encryp_password(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.encryp_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.psw_pass IS NULL THEN
            RAISE EXCEPTION 'La clave no puede ser nula';
        END IF;
        
        -- Remember who changed the payroll when
        NEW.psw_date := current_timestamp;
        NEW.psw_pass := encrypt(NEW.psw_pass,'prueba', 'bf');
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.encryp_password() OWNER TO postgres;

--
-- TOC entry 312 (class 1255 OID 16762)
-- Name: fnc_dictionary_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_dictionary_get_all() RETURNS TABLE(str_name character varying, str_value character varying)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	SELECT 
		dic_name AS str_name,
		dic_value AS str_value
	FROM 
		program.dictionary;
END
 $$;


ALTER FUNCTION public.fnc_dictionary_get_all() OWNER TO postgres;

--
-- TOC entry 299 (class 1255 OID 16660)
-- Name: fnc_get_parameter(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_get_parameter(_key character varying) RETURNS TABLE(valor text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_value AS valor
	FROM 
		program.parameter 
	WHERE 
		par_name = _key;
END
$$;


ALTER FUNCTION public.fnc_get_parameter(_key character varying) OWNER TO postgres;

--
-- TOC entry 310 (class 1255 OID 16763)
-- Name: fnc_men(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men(_prof bigint) RETURNS TABLE(big_id bigint, str_menu character varying, txt_description text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		men_id as big_id, 
		men_name as str_menu, 
		men_description as txt_descripcion 
	FROM 
		program.menu 
	WHERE 
		pro_id = _prof
	ORDER BY
		men_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men(_prof bigint) OWNER TO postgres;

--
-- TOC entry 311 (class 1255 OID 16764)
-- Name: fnc_men_obj(bigint, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men_obj(_prof bigint, _men character varying) RETURNS TABLE(str_object character varying, str_data_function character varying, str_data_parameters character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.menu_options.mop_object as str_object, 
		program.menu_options.mop_data_function as str_data_function,
		program.menu_options.mop_data_parameters::varchar as str_data_parameters
	FROM 
		program.menu_options 
	WHERE 
		program.menu_options.pro_id = _prof AND
		program.menu_options.mop_action = _men AND
		program.menu_options.mop_active = TRUE
	ORDER BY
		men_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men_obj(_prof bigint, _men character varying) OWNER TO postgres;

--
-- TOC entry 313 (class 1255 OID 16765)
-- Name: fnc_men_sub(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_men_sub(_prof bigint, _men bigint) RETURNS TABLE(big_id bigint, big_men_id bigint, str_menu character varying, txt_description text, str_action character varying, str_shortcut character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.menu_options.mop_id as big_id, 
		program.menu_options.men_id as big_men_id,
		program.menu_options.mop_name as str_menu, 
		program.menu_options.mop_description as txt_description,
		program.menu_options.mop_action as str_action,
		program.menu_options.mop_shortcut as str_shortcut
	FROM 
		program.menu_options 
	WHERE 
		program.menu_options.pro_id = _prof AND
		program.menu_options.men_id = _men AND
		program.menu_options.mop_active = TRUE
	ORDER BY
		men_id, mop_id ASC;
END
$$;


ALTER FUNCTION public.fnc_men_sub(_prof bigint, _men bigint) OWNER TO postgres;

--
-- TOC entry 321 (class 1255 OID 16800)
-- Name: fnc_parameter_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de parametro: ', _id, ', ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	DELETE FROM
		program.parameter 
	WHERE
		cast(par_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 294 (class 1255 OID 16766)
-- Name: fnc_parameter_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_get(_id bigint) RETURNS TABLE(big_id bigint, str_name character varying, txt_value text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_id AS big_id,
		par_name AS str_name,
		par_value AS txt_value
	FROM 
		program.parameter 
	WHERE 
		par_id = _id;
END
$$;


ALTER FUNCTION public.fnc_parameter_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 304 (class 1255 OID 16767)
-- Name: fnc_parameter_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_get_all() RETURNS TABLE(big_id bigint, str_name character varying, txt_value text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		par_id AS big_id,
		par_name AS str_name,
		par_value AS txt_value
	FROM 
		program.parameter 
	WHERE 
		par_visible = 1;
END
$$;


ALTER FUNCTION public.fnc_parameter_get_all() OWNER TO postgres;

--
-- TOC entry 303 (class 1255 OID 16788)
-- Name: fnc_parameter_post(character varying, text, smallint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_post(_name character varying, _value text, _visible smallint) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Inserción de parametro: ', _name, ', ', _value)
		);
	INSERT INTO
		program.parameter (
		par_id,
		par_name,
		par_value,
		par_visible
		)
	VALUES (
		nextval('parameter_par_id_seq'),
		_name,
		_value,
		_visible
		);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_post(_name character varying, _value text, _visible smallint) OWNER TO postgres;

--
-- TOC entry 300 (class 1255 OID 16685)
-- Name: fnc_parameter_put(bigint, character varying, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Cambio de parametro: ', _id, ', ', _name, ' de valor ', (select par_value from program.parameter where par_id = _id), ' a ', _value)
		);
	UPDATE 
		program.parameter 
	SET
		par_name = _name,
		par_value = _value
	WHERE
		par_id = _id;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) OWNER TO postgres;

--
-- TOC entry 322 (class 1255 OID 16801)
-- Name: fnc_previews_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de articulo: ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	delete FROM
		program.preview 
	where
		cast(pre_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 320 (class 1255 OID 16798)
-- Name: fnc_previews_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get(_id bigint) RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link as str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview
	WHERE
		pre_id = _id;
END
$$;


ALTER FUNCTION public.fnc_previews_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 305 (class 1255 OID 16768)
-- Name: fnc_previews_get(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get(_link character varying) RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
begin
	RETURN QUERY
	SELECT 
		pre_id AS big_id_id,
		pre_link AS str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview
	WHERE
		lower(pre_link) = lower(_link);
END
$$;


ALTER FUNCTION public.fnc_previews_get(_link character varying) OWNER TO postgres;

--
-- TOC entry 306 (class 1255 OID 16769)
-- Name: fnc_previews_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get_all() RETURNS TABLE(big_id bigint, str_link character varying, txt_title text, txt_article text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link AS str_link,
		pre_title AS txt_title,
		pre_article AS txt_article
	FROM 
		program.preview;
END
$$;


ALTER FUNCTION public.fnc_previews_get_all() OWNER TO postgres;

--
-- TOC entry 307 (class 1255 OID 16770)
-- Name: fnc_previews_get_list(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_get_list() RETURNS TABLE(big_id bigint, str_link character varying)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	SELECT 
		pre_id AS big_id,
		pre_link AS txt_link
	FROM 
		program.preview;
END
$$;


ALTER FUNCTION public.fnc_previews_get_list() OWNER TO postgres;

--
-- TOC entry 308 (class 1255 OID 16739)
-- Name: fnc_previews_post(character varying, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Inserción de articulo: ', _link, ', ', _title, ', ', _article)
		);
	INSERT INTO
		program.preview (
			pre_id,
			pre_link,
			pre_title,
			pre_article
		)
	VALUES (
		nextval('preview_pre_id_seq'),
		_link,
		_title,
		_article
		);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) OWNER TO postgres;

--
-- TOC entry 309 (class 1255 OID 16740)
-- Name: fnc_previews_put(bigint, character varying, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Edición de articulo: ', _id, ' de: ', (select pre_link from program.preview where pre_id = _id), ' a: ' , _link, '; ', (select pre_title from program.preview where pre_id = _id), ' a: ' , _title, ', de: ', (select pre_article from program.preview where pre_id = _id), ' a: ', _article)
		);
	UPDATE
		program.preview 
	set 
		pre_link = _link,
		pre_title = _title,
		pre_article = _article
	where
		pre_id = _id;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) OWNER TO postgres;

--
-- TOC entry 314 (class 1255 OID 16771)
-- Name: fnc_profile_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_profile_get(_pro bigint) RETURNS TABLE(big_id bigint, str_name character varying, big_menu_id bigint, str_menu_name character varying, big_profile_id bigint, str_profile_name character varying, txt_profile_description text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.profile.pro_id as big_id,
		program.profile.pro_name as str_name,
		program.menu.men_id as big_menu_id,
		program.menu.men_name as str_menu_name,
		program.menu_options.mop_id as big_profile_id,
		program.menu_options.mop_name as str_profile_name,
		program.menu_options.mop_description as txt_profile_description 
	FROM 
		program.profile 
		INNER JOIN program.menu ON program.menu.pro_id = program.profile.pro_id
		INNER JOIN program.menu_options ON program.menu_options.men_id = program.menu.men_id
	WHERE
		program.profile.pro_id = _pro
	ORDER BY
		program.menu_options.men_id,
		program.menu_options.mop_id;
END
$$;


ALTER FUNCTION public.fnc_profile_get(_pro bigint) OWNER TO postgres;

--
-- TOC entry 315 (class 1255 OID 16772)
-- Name: fnc_profile_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_profile_get_all() RETURNS TABLE(big_id bigint, str_name character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		program.profile.pro_id as big_id,
		program.profile.pro_name as str_name
	FROM 
		program.profile;
END
$$;


ALTER FUNCTION public.fnc_profile_get_all() OWNER TO postgres;

--
-- TOC entry 316 (class 1255 OID 16773)
-- Name: fnc_table_descript(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_table_descript(_table character varying) RETURNS TABLE(str_column character varying, str_type character varying, int_length integer)
    LANGUAGE plpgsql
    AS $$ 	
BEGIN
	RETURN QUERY
	select 
		CAST(column_name as varchar) as str_column, 
		CAST(data_type as varchar) as str_type, 
		CAST(character_maximum_length as int) as int_length
	from 
		INFORMATION_SCHEMA.COLUMNS 
	where table_name = _table;
END
 $$;


ALTER FUNCTION public.fnc_table_descript(_table character varying) OWNER TO postgres;

--
-- TOC entry 318 (class 1255 OID 16774)
-- Name: fnc_user_pass(character varying, bytea); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_user_pass(_user character varying, _pass bytea) RETURNS TABLE(big_id bigint, big_profile bigint)
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO
		program.log (
		log_date,
		log_time,
		log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Acceso al sistema usuario ', _user)
		);
	RETURN QUERY
	SELECT
		program.user.usr_id as big_id, 
		program.user.pro_id as big_profile
	FROM
		program.user
		INNER JOIN program.user_pass ON program.user.usr_id = program.user_pass.usr_id
	WHERE
		program.user.usr_name = _user AND
		program.user_pass.psw_pass =  _pass
	LIMIT 1;
END
$$;


ALTER FUNCTION public.fnc_user_pass(_user character varying, _pass bytea) OWNER TO postgres;

--
-- TOC entry 296 (class 1255 OID 16671)
-- Name: fnc_user_verify(integer, character varying); Type: FUNCTION; Schema: public; Owner: program
--

CREATE FUNCTION public.fnc_user_verify(_id integer, _user character varying) RETURNS TABLE(result boolean)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT
		CASE WHEN COUNT(usr_id) > 0 THEN true ELSE false END AS result 
	FROM
		program.user
	WHERE
		program.user.usr_id = _id AND
		program.user.usr_name =  _user
	LIMIT 1;
END
$$;


ALTER FUNCTION public.fnc_user_verify(_id integer, _user character varying) OWNER TO program;

--
-- TOC entry 319 (class 1255 OID 16802)
-- Name: fnc_users_delete(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_delete(_id character varying) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
declare
	id_array int[];
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Eliminación de Usuario: ', _id)
		);
	id_array := (string_to_array(_id, ',')::int[]);
	DELETE FROM
		program.user 
	where
		cast(usr_id as int) = any (id_array);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_users_delete(_id character varying) OWNER TO postgres;

--
-- TOC entry 323 (class 1255 OID 16803)
-- Name: fnc_users_get(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_get(_id bigint) RETURNS TABLE(big_id bigint, str_name character varying, bol_active smallint, int_pro bigint, str_pro_name character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		usr_id AS big_id,
		usr_name AS str_name,
		usr_active AS bol_active,
		program.user.pro_id AS big_pro,
		pro_name AS str_pro_name
	FROM 
		program.user  
		INNER JOIN program.profile ON program.profile.pro_id = program.user.pro_id
	WHERE 
		usr_id = _id;
END
$$;


ALTER FUNCTION public.fnc_users_get(_id bigint) OWNER TO postgres;

--
-- TOC entry 317 (class 1255 OID 16776)
-- Name: fnc_users_get_all(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_get_all() RETURNS TABLE(big_id bigint, str_name character varying, bol_active smallint, big_profile bigint, str_profile_name character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT 
		usr_id AS big_id,
		usr_name AS str_name,
		usr_active AS bol_active,
		program.user.pro_id AS big_profile,
		pro_name AS str_profile_name
	FROM 
		program.user  
		INNER JOIN program.profile ON program.profile.pro_id = program.user.pro_id;
END
$$;


ALTER FUNCTION public.fnc_users_get_all() OWNER TO postgres;

--
-- TOC entry 302 (class 1255 OID 16786)
-- Name: fnc_users_post(character varying, smallint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_post(_name character varying, _active smallint, _pro bigint) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Inserción de Usuario: ', _name, ', ', _pro)
		);
	INSERT INTO
		program.user (
			usr_id,
			usr_name,
			usr_active,
			pro_id
		)
	VALUES (
		nextval('program.user_id_usr_seq'),
		_name,
		_active,
		_pro
		);
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_users_post(_name character varying, _active smallint, _pro bigint) OWNER TO postgres;

--
-- TOC entry 301 (class 1255 OID 16712)
-- Name: fnc_users_put(bigint, character varying, smallint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fnc_users_put(_id bigint, _name character varying, _act smallint, _pro bigint) RETURNS TABLE(result text)
    LANGUAGE plpgsql
    AS $$
begin
	INSERT INTO
		program.log (
			log_date,
			log_time,
			log_event
		)
	VALUES (
		CURRENT_DATE,
		CURRENT_TIME,
		CONCAT('Edición de Usuario: ', _id, ', ', _name, ', ', _act, ', ', _pro)
		);
	UPDATE 
		program.user 
	set 
		usr_name = _name,
		usr_active = _act,
		pro_id = _pro
	where
		usr_id = _id;
	RETURN QUERY
	SELECT 't' AS result;
END
$$;


ALTER FUNCTION public.fnc_users_put(_id bigint, _name character varying, _act smallint, _pro bigint) OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 226 (class 1259 OID 16751)
-- Name: dictionary; Type: TABLE; Schema: program; Owner: postgres
--

CREATE TABLE program.dictionary (
    dic_id bigint NOT NULL,
    dic_name character varying(50),
    dic_value character varying(50)
);


ALTER TABLE program.dictionary OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16749)
-- Name: dictionary_dic_id_seq; Type: SEQUENCE; Schema: program; Owner: postgres
--

CREATE SEQUENCE program.dictionary_dic_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.dictionary_dic_id_seq OWNER TO postgres;

--
-- TOC entry 3198 (class 0 OID 0)
-- Dependencies: 225
-- Name: dictionary_dic_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: postgres
--

ALTER SEQUENCE program.dictionary_dic_id_seq OWNED BY program.dictionary.dic_id;


--
-- TOC entry 204 (class 1259 OID 16428)
-- Name: log; Type: TABLE; Schema: program; Owner: postgres
--

CREATE TABLE program.log (
    log_id bigint NOT NULL,
    log_date date,
    log_time time without time zone,
    log_event text
);


ALTER TABLE program.log OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16426)
-- Name: log_log_id_seq; Type: SEQUENCE; Schema: program; Owner: postgres
--

CREATE SEQUENCE program.log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.log_log_id_seq OWNER TO postgres;

--
-- TOC entry 3200 (class 0 OID 0)
-- Dependencies: 203
-- Name: log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: postgres
--

ALTER SEQUENCE program.log_log_id_seq OWNED BY program.log.log_id;


--
-- TOC entry 210 (class 1259 OID 16513)
-- Name: menu; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.menu (
    men_id bigint NOT NULL,
    pro_id bigint NOT NULL,
    men_name character varying(15),
    men_description text
);


ALTER TABLE program.menu OWNER TO rasg;

--
-- TOC entry 3202 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE menu; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON TABLE program.menu IS 'Menús del nivel principal';


--
-- TOC entry 3203 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN menu.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.pro_id IS 'Id del perfil correspondiente';


--
-- TOC entry 3204 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN menu.men_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.men_name IS 'Nombre del menú';


--
-- TOC entry 3205 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN menu.men_description; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu.men_description IS 'Descripción del menú';


--
-- TOC entry 208 (class 1259 OID 16509)
-- Name: menu_men_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_men_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_men_id_seq OWNER TO rasg;

--
-- TOC entry 3207 (class 0 OID 0)
-- Dependencies: 208
-- Name: menu_men_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_men_id_seq OWNED BY program.menu.men_id;


--
-- TOC entry 214 (class 1259 OID 16534)
-- Name: menu_options; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.menu_options (
    mop_id bigint NOT NULL,
    men_id bigint NOT NULL,
    pro_id bigint NOT NULL,
    mop_name character varying(10),
    mop_description text,
    mop_shortcut character varying(10),
    mop_active boolean DEFAULT true,
    mop_object character varying(25),
    mop_data_function character varying(50),
    mop_data_parameters character varying(25)[],
    mop_action character varying(15)
);


ALTER TABLE program.menu_options OWNER TO rasg;

--
-- TOC entry 3208 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE menu_options; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON TABLE program.menu_options IS 'Submenús';


--
-- TOC entry 3209 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_id IS 'Id de los submenús';


--
-- TOC entry 3210 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.men_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.men_id IS 'Menú al que pertenece';


--
-- TOC entry 3211 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.pro_id IS 'Id del perfil al que pertenece';


--
-- TOC entry 3212 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_name IS 'Nombre del submenu';


--
-- TOC entry 3213 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_description; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_description IS 'Descripción del submenú';


--
-- TOC entry 3214 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_shortcut; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_shortcut IS 'Teclas del acceso directo';


--
-- TOC entry 3215 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_object; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_object IS 'Objeto de uso';


--
-- TOC entry 3216 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN menu_options.mop_data_function; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.menu_options.mop_data_function IS 'Funcion en la base de datos';


--
-- TOC entry 212 (class 1259 OID 16530)
-- Name: menu_options_men_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_men_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_men_id_seq OWNER TO rasg;

--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 212
-- Name: menu_options_men_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_men_id_seq OWNED BY program.menu_options.men_id;


--
-- TOC entry 211 (class 1259 OID 16528)
-- Name: menu_options_mop_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_mop_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_mop_id_seq OWNER TO rasg;

--
-- TOC entry 3219 (class 0 OID 0)
-- Dependencies: 211
-- Name: menu_options_mop_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_mop_id_seq OWNED BY program.menu_options.mop_id;


--
-- TOC entry 213 (class 1259 OID 16532)
-- Name: menu_options_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_options_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_options_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3220 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_options_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_options_pro_id_seq OWNED BY program.menu_options.pro_id;


--
-- TOC entry 209 (class 1259 OID 16511)
-- Name: menu_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.menu_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.menu_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 209
-- Name: menu_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.menu_pro_id_seq OWNED BY program.menu.pro_id;


--
-- TOC entry 202 (class 1259 OID 16420)
-- Name: parameter; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.parameter (
    par_id bigint NOT NULL,
    par_name character varying(15),
    par_value text,
    par_visible smallint DEFAULT 0 NOT NULL
);


ALTER TABLE program.parameter OWNER TO rasg;

--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN parameter.par_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.parameter.par_id IS 'Id correspondiente al parametro';


--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN parameter.par_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.parameter.par_name IS 'Nombre del parametro';


--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN parameter.par_value; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.parameter.par_value IS 'Valor del parametro';


--
-- TOC entry 201 (class 1259 OID 16418)
-- Name: parameter_par_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.parameter_par_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.parameter_par_id_seq OWNER TO rasg;

--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 201
-- Name: parameter_par_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.parameter_par_id_seq OWNED BY program.parameter.par_id;


--
-- TOC entry 223 (class 1259 OID 16687)
-- Name: preview; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.preview (
    pre_id bigint NOT NULL,
    pre_title text,
    pre_article text,
    pre_link character varying(50)
);


ALTER TABLE program.preview OWNER TO rasg;

--
-- TOC entry 3228 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN preview.pre_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_id IS 'Id interno de la tabla preview';


--
-- TOC entry 3229 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN preview.pre_title; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_title IS 'Titulo del articulo';


--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN preview.pre_article; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.preview.pre_article IS 'Articulo en HTML';


--
-- TOC entry 224 (class 1259 OID 16690)
-- Name: preview_pre_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.preview_pre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.preview_pre_id_seq OWNER TO rasg;

--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 224
-- Name: preview_pre_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.preview_pre_id_seq OWNED BY program.preview.pre_id;


--
-- TOC entry 206 (class 1259 OID 16487)
-- Name: profile; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.profile (
    pro_id bigint NOT NULL,
    pro_name character varying(15)
);


ALTER TABLE program.profile OWNER TO rasg;

--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN profile.pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.profile.pro_id IS 'Id del perfil';


--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN profile.pro_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.profile.pro_name IS 'Nombre del perfil';


--
-- TOC entry 205 (class 1259 OID 16485)
-- Name: profile_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.profile_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.profile_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3237 (class 0 OID 0)
-- Dependencies: 205
-- Name: profile_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.profile_pro_id_seq OWNED BY program.profile.pro_id;


--
-- TOC entry 200 (class 1259 OID 16411)
-- Name: user; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program."user" (
    usr_id bigint NOT NULL,
    usr_name character varying(20) NOT NULL,
    usr_active smallint DEFAULT 0 NOT NULL,
    pro_id bigint NOT NULL
);


ALTER TABLE program."user" OWNER TO rasg;

--
-- TOC entry 3239 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN "user".usr_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_id IS 'Id de usuario';


--
-- TOC entry 3240 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN "user".usr_name; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_name IS 'Usuario del sistema';


--
-- TOC entry 3241 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN "user".usr_active; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".usr_active IS '0 Usuario Inactivo; 1 Usuario Activo';


--
-- TOC entry 3242 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN "user".pro_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program."user".pro_id IS 'Id del perfil';


--
-- TOC entry 199 (class 1259 OID 16409)
-- Name: user_id_usr_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_id_usr_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_id_usr_seq OWNER TO rasg;

--
-- TOC entry 3244 (class 0 OID 0)
-- Dependencies: 199
-- Name: user_id_usr_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_id_usr_seq OWNED BY program."user".usr_id;


--
-- TOC entry 216 (class 1259 OID 16569)
-- Name: user_pass; Type: TABLE; Schema: program; Owner: rasg
--

CREATE TABLE program.user_pass (
    psw_id bigint NOT NULL,
    usr_id bigint NOT NULL,
    psw_date timestamp without time zone NOT NULL,
    psw_pass bytea
);


ALTER TABLE program.user_pass OWNER TO rasg;

--
-- TOC entry 3246 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN user_pass.psw_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_id IS 'Id de la tabla password';


--
-- TOC entry 3247 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN user_pass.usr_id; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.usr_id IS 'Id del usuario';


--
-- TOC entry 3248 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN user_pass.psw_date; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_date IS 'Fecha y hora de creación del password';


--
-- TOC entry 3249 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN user_pass.psw_pass; Type: COMMENT; Schema: program; Owner: rasg
--

COMMENT ON COLUMN program.user_pass.psw_pass IS 'Password cifrado';


--
-- TOC entry 215 (class 1259 OID 16567)
-- Name: user_pass_psw_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pass_psw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pass_psw_id_seq OWNER TO rasg;

--
-- TOC entry 3251 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_pass_psw_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pass_psw_id_seq OWNED BY program.user_pass.psw_id;


--
-- TOC entry 217 (class 1259 OID 16575)
-- Name: user_pass_usr_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pass_usr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pass_usr_id_seq OWNER TO rasg;

--
-- TOC entry 3252 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_pass_usr_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pass_usr_id_seq OWNED BY program.user_pass.usr_id;


--
-- TOC entry 207 (class 1259 OID 16493)
-- Name: user_pro_id_seq; Type: SEQUENCE; Schema: program; Owner: rasg
--

CREATE SEQUENCE program.user_pro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE program.user_pro_id_seq OWNER TO rasg;

--
-- TOC entry 3253 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_pro_id_seq; Type: SEQUENCE OWNED BY; Schema: program; Owner: rasg
--

ALTER SEQUENCE program.user_pro_id_seq OWNED BY program."user".pro_id;


--
-- TOC entry 3015 (class 2604 OID 16754)
-- Name: dictionary dic_id; Type: DEFAULT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.dictionary ALTER COLUMN dic_id SET DEFAULT nextval('program.dictionary_dic_id_seq'::regclass);


--
-- TOC entry 3004 (class 2604 OID 16431)
-- Name: log log_id; Type: DEFAULT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.log ALTER COLUMN log_id SET DEFAULT nextval('program.log_log_id_seq'::regclass);


--
-- TOC entry 3006 (class 2604 OID 16516)
-- Name: menu men_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu ALTER COLUMN men_id SET DEFAULT nextval('program.menu_men_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 16517)
-- Name: menu pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu ALTER COLUMN pro_id SET DEFAULT nextval('program.menu_pro_id_seq'::regclass);


--
-- TOC entry 3008 (class 2604 OID 16537)
-- Name: menu_options mop_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN mop_id SET DEFAULT nextval('program.menu_options_mop_id_seq'::regclass);


--
-- TOC entry 3009 (class 2604 OID 16538)
-- Name: menu_options men_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN men_id SET DEFAULT nextval('program.menu_options_men_id_seq'::regclass);


--
-- TOC entry 3010 (class 2604 OID 16539)
-- Name: menu_options pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options ALTER COLUMN pro_id SET DEFAULT nextval('program.menu_options_pro_id_seq'::regclass);


--
-- TOC entry 3002 (class 2604 OID 16423)
-- Name: parameter par_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.parameter ALTER COLUMN par_id SET DEFAULT nextval('program.parameter_par_id_seq'::regclass);


--
-- TOC entry 3014 (class 2604 OID 16692)
-- Name: preview pre_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.preview ALTER COLUMN pre_id SET DEFAULT nextval('program.preview_pre_id_seq'::regclass);


--
-- TOC entry 3005 (class 2604 OID 16490)
-- Name: profile pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.profile ALTER COLUMN pro_id SET DEFAULT nextval('program.profile_pro_id_seq'::regclass);


--
-- TOC entry 2999 (class 2604 OID 16414)
-- Name: user usr_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user" ALTER COLUMN usr_id SET DEFAULT nextval('program.user_id_usr_seq'::regclass);


--
-- TOC entry 3001 (class 2604 OID 16495)
-- Name: user pro_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user" ALTER COLUMN pro_id SET DEFAULT nextval('program.user_pro_id_seq'::regclass);


--
-- TOC entry 3012 (class 2604 OID 16572)
-- Name: user_pass psw_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass ALTER COLUMN psw_id SET DEFAULT nextval('program.user_pass_psw_id_seq'::regclass);


--
-- TOC entry 3013 (class 2604 OID 16577)
-- Name: user_pass usr_id; Type: DEFAULT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass ALTER COLUMN usr_id SET DEFAULT nextval('program.user_pass_usr_id_seq'::regclass);


--
-- TOC entry 3185 (class 0 OID 16751)
-- Dependencies: 226
-- Data for Name: dictionary; Type: TABLE DATA; Schema: program; Owner: postgres
--

INSERT INTO program.dictionary VALUES (9, 'user', 'Usuarios');
INSERT INTO program.dictionary VALUES (10, 'profile', 'Perfiles');
INSERT INTO program.dictionary VALUES (12, 'parameter', 'Parametros');
INSERT INTO program.dictionary VALUES (15, 'pro_id', 'Perfil');
INSERT INTO program.dictionary VALUES (11, 'item', 'Vistas');
INSERT INTO program.dictionary VALUES (1, 'str_name', 'Nombre');
INSERT INTO program.dictionary VALUES (3, 'str_title', 'Titulo');
INSERT INTO program.dictionary VALUES (4, 'str_link', 'Enlace');
INSERT INTO program.dictionary VALUES (5, 'txt_article', 'Articulo');
INSERT INTO program.dictionary VALUES (6, 'str_active', 'Activo');
INSERT INTO program.dictionary VALUES (13, 'str_name', 'Nombre');
INSERT INTO program.dictionary VALUES (14, 'bol_active', 'Activar');
INSERT INTO program.dictionary VALUES (16, 'str_name', 'Nombre Perfil');
INSERT INTO program.dictionary VALUES (17, 'txt_title', 'Titulo');
INSERT INTO program.dictionary VALUES (18, 'txt_article', 'HTML Cuerpo');
INSERT INTO program.dictionary VALUES (19, 'str_link', 'Enlace');
INSERT INTO program.dictionary VALUES (2, 'txt_value', 'Valor');
INSERT INTO program.dictionary VALUES (20, 'preview', 'Portada');
INSERT INTO program.dictionary VALUES (21, 'big_id', 'ID');
INSERT INTO program.dictionary VALUES (22, 'par_name', 'Nombre');
INSERT INTO program.dictionary VALUES (23, 'par_value', 'Valor');
INSERT INTO program.dictionary VALUES (24, 'par_visible', 'Visible');
INSERT INTO program.dictionary VALUES (25, 'usr_name', 'Nombre Usuario');
INSERT INTO program.dictionary VALUES (26, 'usr_active', 'Usuario Activo');
INSERT INTO program.dictionary VALUES (27, 'pro_name', 'Nombre Perfil');
INSERT INTO program.dictionary VALUES (28, 'pre_title', 'Titulo');
INSERT INTO program.dictionary VALUES (29, 'pre_article', 'Articulo');
INSERT INTO program.dictionary VALUES (30, 'pre_link', 'Enlace');
INSERT INTO program.dictionary VALUES (8, 'str_pro_name', 'Nombre Perfil');
INSERT INTO program.dictionary VALUES (7, 'int_pro', 'Perfil');


--
-- TOC entry 3168 (class 0 OID 16428)
-- Dependencies: 204
-- Data for Name: log; Type: TABLE DATA; Schema: program; Owner: postgres
--

INSERT INTO program.log VALUES (1, '2019-07-14', '18:48:28.217742', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (2, '2019-08-04', '12:39:55.917036', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (4, '2019-08-04', '13:08:38.353502', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (5, '2019-08-04', '13:14:29.34779', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (6, '2019-08-11', '13:12:51.731441', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (7, '2019-08-11', '13:16:08.171862', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (8, '2019-08-11', '13:18:39.459455', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (9, '2019-08-11', '13:23:17.593264', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (10, '2019-08-11', '13:25:36.401066', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (11, '2019-08-11', '13:27:31.047828', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (12, '2019-08-11', '13:28:21.7025', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (13, '2019-08-11', '13:51:32.632638', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (14, '2019-08-11', '14:01:26.708336', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (15, '2019-08-11', '14:03:22.463325', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (16, '2019-08-11', '14:03:41.65361', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (17, '2019-08-11', '14:04:20.857682', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (18, '2019-08-11', '14:04:23.394927', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (19, '2019-08-11', '14:05:04.148899', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (20, '2019-08-11', '14:06:57.903084', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (21, '2019-08-11', '14:07:25.176994', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (22, '2019-08-11', '14:08:23.243987', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (23, '2019-08-11', '14:11:01.758764', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (24, '2019-08-11', '14:12:02.462265', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (25, '2019-08-11', '14:13:55.564969', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (26, '2019-08-11', '14:19:36.347912', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (27, '2019-08-11', '14:20:02.800939', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (28, '2019-08-11', '14:20:23.74491', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (29, '2019-08-11', '14:21:25.984408', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (30, '2019-08-12', '20:34:17.036146', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (31, '2019-08-12', '20:34:30.909787', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (32, '2019-08-12', '20:36:30.351838', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (33, '2019-08-12', '20:38:27.34321', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (34, '2019-08-12', '20:38:47.945147', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (35, '2019-08-12', '20:42:24.039258', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (36, '2019-08-12', '20:44:29.278542', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (37, '2019-08-18', '09:56:24.462556', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (38, '2019-08-18', '10:19:37.762429', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (39, '2019-08-18', '10:22:14.022525', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (40, '2019-08-18', '10:23:09.952071', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (41, '2019-08-18', '10:24:53.281491', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (42, '2019-08-18', '10:25:26.722785', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (43, '2019-08-18', '10:33:27.265809', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (44, '2019-08-18', '10:34:23.199478', 'Acceso al sistema usuario ');
INSERT INTO program.log VALUES (45, '2019-08-18', '10:34:57.551256', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (46, '2019-08-18', '12:16:55.071214', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (47, '2019-08-18', '12:17:30.694178', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (48, '2019-08-18', '12:18:29.613314', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (49, '2019-08-18', '12:23:27.120377', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (50, '2019-08-18', '12:25:52.097443', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (51, '2019-08-18', '12:26:34.472461', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (52, '2019-08-18', '12:40:49.972745', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (53, '2019-08-18', '12:42:40.208456', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (54, '2019-08-18', '12:43:56.561505', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (55, '2019-08-18', '12:44:49.375733', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (56, '2019-08-18', '12:46:15.603113', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (57, '2019-08-18', '12:48:51.063423', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (58, '2019-08-18', '12:52:20.924786', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (59, '2019-08-18', '13:54:56.07288', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (60, '2019-08-25', '11:25:59.036845', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (61, '2019-08-25', '11:29:37.187825', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (62, '2019-08-25', '15:10:30.983009', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (63, '2019-08-25', '15:15:10.801517', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (64, '2019-08-25', '15:16:21.3687', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (65, '2019-08-25', '15:17:11.067307', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (66, '2019-08-25', '15:18:07.334508', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (67, '2019-08-25', '15:19:47.516662', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (68, '2019-08-25', '16:07:16.95342', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (69, '2019-08-25', '16:32:22.2377', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (70, '2019-08-25', '16:41:38.599968', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (71, '2019-08-25', '16:44:49.040485', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (72, '2019-08-25', '16:51:05.596944', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (73, '2019-08-25', '17:20:08.863514', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (74, '2019-09-01', '21:37:54.230191', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (75, '2019-09-01', '21:40:19.096932', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (76, '2019-09-01', '21:45:14.279768', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (77, '2019-09-04', '20:52:40.648679', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (78, '2019-09-08', '08:58:56.900275', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (79, '2019-09-08', '09:00:27.851442', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (80, '2019-09-08', '10:05:01.010471', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (81, '2019-09-08', '10:57:05.102178', 'User, insertar Usuario rasg, perfil: 0');
INSERT INTO program.log VALUES (82, '2019-09-08', '11:04:40.147286', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (83, '2019-09-08', '11:05:14.345207', 'Inserción de parametro: admin, rastrahm');
INSERT INTO program.log VALUES (84, '2019-09-08', '11:14:10.685035', 'Cambio de parametro: 13, admin de valor rastrahm a admin');
INSERT INTO program.log VALUES (85, '2019-09-08', '11:15:48.514706', 'Cambio de parametro: 12, admin de valor  a rasg');
INSERT INTO program.log VALUES (86, '2019-09-08', '11:23:06.441258', 'Inserción de Usuario: prueba, 0');
INSERT INTO program.log VALUES (87, '2019-09-08', '11:23:06.441258', 'User, insertar Usuario prueba, perfil: 0');
INSERT INTO program.log VALUES (91, '2019-09-08', '12:21:12.444589', 'Inserción de articulo: prueba, prueba');
INSERT INTO program.log VALUES (92, '2019-09-08', '12:25:26.625183', 'Edición de articulo: 1 de: prueba a: prueba, de: prueba a: prueba 2');
INSERT INTO program.log VALUES (93, '2019-09-18', '21:10:52.852147', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (94, '2019-09-18', '21:24:55.003047', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (95, '2019-09-21', '20:42:21.033012', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (96, '2019-09-22', '10:33:38.55851', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (97, '2019-10-06', '09:06:23.562351', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (98, '2019-10-06', '09:09:27.673082', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (99, '2019-10-06', '09:10:45.652423', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (100, '2019-10-06', '09:23:55.782928', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (101, '2019-10-06', '09:40:06.461346', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (102, '2019-10-06', '09:42:00.489722', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (103, '2019-10-06', '09:45:13.776045', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (104, '2019-10-06', '09:58:11.061763', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (105, '2019-10-06', '10:02:03.212344', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (106, '2019-10-06', '10:09:10.962778', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (107, '2019-10-06', '10:16:30.804895', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (108, '2019-10-06', '10:26:03.642803', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (109, '2019-10-06', '10:29:16.375891', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (110, '2019-10-06', '10:30:19.861106', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (111, '2019-10-06', '10:31:06.28664', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (112, '2019-10-06', '10:31:14.137945', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (113, '2019-10-06', '10:38:57.05183', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (114, '2019-10-06', '10:39:16.956298', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (115, '2019-10-06', '10:41:07.992965', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (116, '2019-10-06', '10:41:23.343308', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (117, '2019-10-06', '10:41:56.907482', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (118, '2019-10-06', '10:43:06.09611', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (119, '2019-10-06', '10:53:38.472799', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (120, '2019-10-06', '10:59:23.598368', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (121, '2019-10-06', '11:00:01.311651', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (122, '2019-10-06', '11:00:07.545991', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (123, '2019-10-06', '11:02:53.962405', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (124, '2019-10-06', '11:16:00.47988', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (125, '2019-10-06', '11:25:20.993036', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (126, '2019-10-06', '11:29:17.296981', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (127, '2019-10-06', '11:32:26.826963', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (128, '2019-10-13', '10:08:46.296413', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (129, '2019-10-13', '10:09:11.07265', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (130, '2019-10-13', '11:25:17.494558', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (131, '2019-10-13', '11:30:50.074009', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (132, '2019-10-13', '11:31:14.520895', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (133, '2019-10-13', '11:36:48.614304', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (134, '2019-10-13', '11:43:31.730097', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (135, '2019-10-13', '11:56:30.253011', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (136, '2019-10-13', '12:00:22.839383', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (137, '2019-10-13', '12:03:30.406228', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (138, '2019-10-13', '12:20:01.031959', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (139, '2019-10-13', '12:47:21.421895', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (140, '2019-10-13', '12:53:25.002329', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (141, '2019-10-13', '12:59:29.49051', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (142, '2019-10-13', '13:04:56.312907', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (143, '2019-10-13', '19:32:08.940086', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (144, '2019-10-13', '19:38:08.391375', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (145, '2019-10-13', '19:46:56.576706', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (146, '2019-10-13', '19:50:07.835389', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (147, '2019-10-13', '19:52:46.424887', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (148, '2019-10-13', '19:56:50.150728', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (149, '2019-10-13', '20:15:55.746698', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (150, '2019-10-13', '20:39:46.865301', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (151, '2019-10-13', '20:41:29.79069', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (152, '2019-10-13', '20:43:29.897427', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (153, '2019-10-13', '20:46:30.311374', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (154, '2019-10-14', '20:37:44.31446', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (155, '2019-10-14', '20:43:53.184977', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (156, '2019-10-14', '20:50:46.383973', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (157, '2019-10-14', '20:52:43.690493', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (158, '2019-10-14', '20:53:46.999119', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (159, '2019-10-14', '21:10:54.853625', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (160, '2019-10-14', '21:13:47.008017', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (161, '2019-10-14', '21:16:26.884635', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (162, '2019-10-14', '21:17:26.551275', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (163, '2019-10-14', '21:20:43.199428', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (164, '2019-10-15', '21:03:55.819657', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (165, '2019-10-15', '21:06:02.254425', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (166, '2019-10-15', '21:06:50.574626', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (167, '2019-10-15', '21:17:50.313336', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (168, '2019-10-15', '21:18:51.278937', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (169, '2019-10-15', '21:20:30.307838', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (170, '2019-10-15', '21:22:18.730024', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (171, '2019-10-15', '21:24:44.249221', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (172, '2019-10-15', '21:25:55.305546', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (173, '2019-10-15', '21:29:12.080545', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (174, '2019-10-15', '21:34:02.861658', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (175, '2019-10-15', '21:35:52.361768', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (176, '2019-10-15', '21:38:25.360621', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (177, '2019-10-20', '09:42:09.589494', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (178, '2019-10-20', '09:43:14.390821', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (179, '2019-10-20', '09:44:12.826831', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (180, '2019-10-20', '10:02:15.843523', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (181, '2019-10-20', '10:03:48.079301', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (182, '2019-10-20', '10:11:49.036481', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (183, '2019-10-20', '10:34:57.511545', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (184, '2019-10-20', '11:33:20.417462', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (185, '2019-10-20', '11:39:48.95749', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (186, '2019-10-20', '12:08:30.664981', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (187, '2019-10-20', '12:11:23.745932', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (188, '2019-10-20', '13:15:01.919697', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (189, '2019-10-20', '13:39:34.781291', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (190, '2019-10-20', '13:57:49.286627', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (191, '2019-10-20', '14:08:21.717402', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (192, '2019-10-20', '14:19:53.888273', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (193, '2019-10-20', '14:23:31.004333', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (194, '2019-10-20', '14:42:22.109554', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (195, '2019-10-20', '14:55:44.342161', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (196, '2019-10-20', '15:02:20.620738', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (197, '2019-10-20', '15:39:28.708936', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (198, '2019-10-20', '15:48:52.190096', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (199, '2019-10-20', '15:55:18.077135', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (200, '2019-10-20', '16:04:56.768656', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (201, '2019-10-20', '16:06:56.935754', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (202, '2019-10-20', '16:09:29.788244', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (203, '2019-10-20', '16:13:43.849856', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (204, '2019-10-20', '16:20:55.605956', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (205, '2019-10-20', '16:26:49.090014', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (206, '2019-10-20', '16:33:53.908154', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (207, '2019-10-20', '16:55:00.324771', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (208, '2019-10-20', '16:58:53.695778', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (209, '2019-10-20', '17:07:19.161226', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (210, '2019-10-20', '17:12:08.484849', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (211, '2019-10-20', '17:28:08.343048', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (212, '2019-10-20', '17:32:02.849273', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (213, '2019-10-20', '17:38:38.368884', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (214, '2019-10-20', '17:41:02.614807', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (215, '2019-10-20', '17:46:35.028958', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (222, '2019-10-27', '10:07:51.629378', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (223, '2019-10-27', '10:08:07.556166', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (224, '2019-10-27', '10:09:39.968275', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (225, '2019-10-27', '10:15:34.858726', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (226, '2019-10-27', '10:25:12.683441', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (227, '2019-10-27', '10:27:13.473079', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (228, '2019-10-27', '10:43:33.244339', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (229, '2019-10-27', '10:55:34.384422', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (230, '2019-10-27', '11:44:00.729387', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (231, '2019-10-27', '11:48:54.774606', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (232, '2019-10-27', '12:00:47.570105', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (233, '2019-10-27', '12:11:11.776257', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (234, '2019-10-27', '12:42:19.520018', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (235, '2019-10-27', '13:51:47.137218', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (236, '2019-10-27', '14:03:27.57145', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (237, '2019-10-27', '14:09:13.405307', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (238, '2019-10-27', '18:29:17.078523', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (239, '2019-10-27', '18:39:42.706891', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (240, '2019-10-27', '18:48:49.534313', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (241, '2019-10-27', '19:02:31.176258', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (242, '2019-10-27', '20:09:41.652747', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (243, '2019-10-27', '20:23:56.313283', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (244, '2019-10-27', '20:31:20.207241', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (245, '2019-10-27', '20:34:25.297115', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (246, '2019-10-27', '20:35:40.730228', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (247, '2019-10-27', '20:39:52.542458', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (248, '2019-10-28', '20:59:49.070742', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (249, '2019-10-28', '21:01:16.602606', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (250, '2019-10-28', '21:07:59.906033', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (251, '2019-10-28', '21:11:13.660045', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (252, '2019-10-28', '21:16:05.252314', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (253, '2019-10-28', '21:21:18.838214', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (254, '2019-10-28', '21:23:03.178836', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (255, '2019-10-28', '21:36:00.715435', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (256, '2019-10-28', '21:48:25.316762', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (257, '2019-10-28', '21:49:34.354263', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (258, '2019-10-28', '21:51:28.666865', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (259, '2019-10-30', '21:27:14.16112', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (260, '2019-10-30', '21:43:56.47612', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (261, '2019-10-30', '21:44:00.56818', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (262, '2019-10-30', '21:47:57.998454', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (263, '2019-10-30', '21:51:01.316522', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (264, '2019-10-30', '21:56:02.207922', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (265, '2019-10-30', '22:01:12.725814', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (266, '2019-11-03', '09:05:08.518839', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (267, '2019-11-03', '09:08:40.589194', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (268, '2019-11-03', '09:13:58.850034', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (269, '2019-11-03', '09:49:48.060795', 'Inserción de parametro: prueba, valor');
INSERT INTO program.log VALUES (270, '2019-11-03', '09:58:40.525137', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (271, '2019-11-03', '10:00:06.388623', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (272, '2019-11-03', '10:04:25.862737', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (273, '2019-11-03', '10:11:19.242799', 'Inserción de parametro: prueba 1, valor 1');
INSERT INTO program.log VALUES (274, '2019-11-03', '10:13:09.006742', 'Inserción de parametro: prueba 2, valor 2');
INSERT INTO program.log VALUES (275, '2019-11-03', '10:20:13.734857', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (276, '2019-11-03', '10:23:33.363298', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (277, '2019-11-03', '10:51:28.013127', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (278, '2019-11-03', '11:49:04.580882', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (279, '2019-11-03', '11:53:03.0516', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (280, '2019-11-03', '11:57:19.198819', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (281, '2019-11-03', '12:08:13.379708', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (282, '2019-11-03', '12:19:09.703154', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (283, '2019-11-03', '12:23:17.506647', 'Inserción de Usuario: prueba 1, 0');
INSERT INTO program.log VALUES (284, '2019-11-03', '12:23:17.506647', 'User, insertar Usuario prueba 1, perfil: 0');
INSERT INTO program.log VALUES (285, '2019-11-03', '12:24:31.621351', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (286, '2019-11-03', '12:25:24.780405', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (287, '2019-11-03', '12:28:41.033139', 'Inserción de Usuario: prueba 2, 0');
INSERT INTO program.log VALUES (288, '2019-11-03', '12:28:41.033139', 'User, insertar Usuario prueba 2, perfil: 0');
INSERT INTO program.log VALUES (289, '2019-11-03', '12:54:21.183441', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (290, '2019-11-03', '14:45:06.475517', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (293, '2019-11-03', '14:51:22.943592', 'Inserción de Usuario: prueba 3, 0');
INSERT INTO program.log VALUES (294, '2019-11-03', '14:51:22.943592', 'User, insertar Usuario prueba 3, perfil: 0');
INSERT INTO program.log VALUES (295, '2019-11-03', '15:06:54.26011', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (296, '2019-11-03', '15:09:29.745047', 'Inserción de parametro: prueba 3, valor 3');
INSERT INTO program.log VALUES (297, '2019-11-03', '15:10:04.924618', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (298, '2019-11-03', '15:11:39.158458', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (299, '2019-11-03', '15:20:59.663539', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (300, '2019-11-03', '15:22:33.379062', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (301, '2019-11-03', '15:24:01.603467', 'Inserción de Usuario: prueba 5, 0');
INSERT INTO program.log VALUES (302, '2019-11-03', '15:24:01.603467', 'User, insertar Usuario prueba 5, perfil: 0');
INSERT INTO program.log VALUES (303, '2019-11-03', '15:31:51.360362', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (304, '2019-11-03', '15:40:32.207347', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (305, '2019-11-03', '15:43:28.467133', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (306, '2019-11-03', '15:44:57.787063', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (307, '2019-11-03', '19:01:11.628376', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (309, '2019-11-03', '19:10:00.132237', 'Inserción de articulo: Prueba, &lt;h1&gt;Prueba&lt;/h1&gt;, &lt;p&gt;Prueba de Articulos&lt;/p&gt;');
INSERT INTO program.log VALUES (310, '2019-11-03', '19:12:55.130032', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (311, '2019-11-03', '19:16:12.191523', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (312, '2019-11-04', '20:58:06.668579', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (313, '2019-11-04', '20:58:10.502832', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (314, '2019-11-10', '10:14:22.765407', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (315, '2019-11-10', '10:25:02.92884', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (316, '2019-11-10', '10:37:53.795198', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (317, '2019-11-10', '10:43:27.810188', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (318, '2019-11-10', '14:30:02.65895', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (319, '2019-11-10', '14:46:15.353784', 'Inserción de articulo: Articulo, Articulo, Articulo');
INSERT INTO program.log VALUES (320, '2019-11-10', '14:48:15.304382', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (321, '2019-11-10', '14:55:06.5532', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (322, '2019-11-10', '14:57:21.053147', 'Inserción de articulo: 4 Prueba, Prueba 4, Prueba 4');
INSERT INTO program.log VALUES (323, '2019-11-10', '15:00:36.166515', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (324, '2019-11-11', '13:13:53.423277', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (325, '2019-11-11', '13:21:32.856419', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (326, '2019-11-11', '13:22:50.699406', 'Inserción de articulo: Debugger, Debugger, Debugger');
INSERT INTO program.log VALUES (327, '2019-11-11', '13:26:58.591564', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (328, '2019-11-11', '13:28:59.067859', 'Inserción de articulo: Depurar, Depurar, Depuración del sistema');
INSERT INTO program.log VALUES (329, '2019-11-11', '13:54:21.621773', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (330, '2019-11-11', '13:56:06.555154', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (331, '2019-11-11', '13:57:21.286458', 'Inserción de articulo: event.preventDefault();, event.preventDefault();, event.preventDefault();');
INSERT INTO program.log VALUES (332, '2019-11-11', '14:09:06.210111', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (333, '2019-11-11', '14:10:37.768549', 'Inserción de articulo: Portada, Portada, Portada Articulo');
INSERT INTO program.log VALUES (334, '2019-11-11', '14:14:58.629947', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (335, '2019-11-11', '14:17:19.041741', 'Inserción de articulo: Articulo, Articulo, Articulo de prueba, sin referencia real');
INSERT INTO program.log VALUES (336, '2019-11-11', '15:08:21.028975', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (337, '2019-11-11', '16:01:03.547495', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Codigo</h1> a: <h1>Codigo</h1>, de: Prueba a: <h3>Prueba</h3>
<p>Texto de prueba</p>');
INSERT INTO program.log VALUES (338, '2019-11-11', '16:04:22.816189', 'Inserción de parametro: Prueba 4, Valor 4');
INSERT INTO program.log VALUES (339, '2019-11-11', '16:06:06.075186', 'Cambio de parametro: 17, Prueba 3 de valor valor 3 a Valor 3');
INSERT INTO program.log VALUES (340, '2019-11-11', '16:19:21.230967', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (341, '2019-11-11', '16:24:45.535605', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (342, '2019-11-11', '16:28:41.205279', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (343, '2019-11-11', '16:32:48.665313', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (344, '2019-11-11', '16:44:43.375498', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (345, '2019-11-11', '16:45:31.171698', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (346, '2019-11-11', '16:48:12.382922', 'Edición de Usuario: 3, prueba, 1, 0');
INSERT INTO program.log VALUES (347, '2019-11-11', '16:48:12.382922', 'User, actualización Usuario: prueba, Activo: 1, Perfil: 0');
INSERT INTO program.log VALUES (348, '2019-11-11', '16:52:41.291616', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Codigo</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Prueba</h3>
<p>Texto de prueba</p> a: <h3>Construcción del Código</h3>
<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p>');
INSERT INTO program.log VALUES (349, '2019-11-11', '16:53:42.513902', 'Cambio de parametro: 16, Prueba 2 de valor valor 2 a Valor 2');
INSERT INTO program.log VALUES (350, '2019-11-11', '17:37:09.729617', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (351, '2019-11-11', '17:39:47.010278', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (352, '2019-11-11', '17:42:05.098962', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (353, '2019-11-11', '17:55:29.295131', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (354, '2019-11-11', '17:59:11.860093', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (355, '2019-11-12', '13:30:06.172277', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (356, '2019-11-12', '14:13:37.267032', 'Eliminación de parametro: 18, 17, 18, 17');
INSERT INTO program.log VALUES (357, '2019-11-12', '14:14:18.852933', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (358, '2019-11-12', '14:15:04.819073', 'Eliminación de parametro: 15,16, 15,16');
INSERT INTO program.log VALUES (359, '2019-11-12', '14:18:49.561982', 'Eliminación de articulo: 10');
INSERT INTO program.log VALUES (361, '2019-11-12', '14:26:57.482752', 'Eliminación de Usuario: 4,5,6,7,3');
INSERT INTO program.log VALUES (362, '2019-11-12', '14:28:26.491761', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (363, '2019-11-12', '14:30:38.866756', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (364, '2019-11-12', '14:50:43.268416', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (365, '2019-11-12', '14:51:59.609157', 'Inserción de Usuario: prueba, 0');
INSERT INTO program.log VALUES (366, '2019-11-12', '14:51:59.609157', 'User, insertar Usuario prueba, perfil: 0');
INSERT INTO program.log VALUES (367, '2019-11-12', '14:59:41.443555', 'Edición de Usuario: 2, rasg, 1, 0');
INSERT INTO program.log VALUES (368, '2019-11-12', '14:59:41.443555', 'User, actualización Usuario: rasg, Activo: 1, Perfil: 0');
INSERT INTO program.log VALUES (369, '2019-11-12', '15:00:20.294611', 'Inserción de Usuario: prueba 1, 0');
INSERT INTO program.log VALUES (370, '2019-11-12', '15:00:20.294611', 'User, insertar Usuario prueba 1, perfil: 0');
INSERT INTO program.log VALUES (373, '2019-11-12', '15:08:45.591056', 'Eliminación de Usuario: 8,9');
INSERT INTO program.log VALUES (374, '2019-11-12', '15:26:32.877496', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (375, '2019-11-12', '15:28:32.271683', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (376, '2019-11-12', '15:33:40.222604', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (377, '2019-11-12', '15:36:41.772048', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (378, '2019-11-12', '15:48:25.009743', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (379, '2019-11-12', '15:57:21.138633', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (380, '2019-11-12', '15:59:36.618698', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (381, '2019-11-12', '16:01:23.937274', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (382, '2019-11-12', '16:09:20.494491', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (383, '2019-11-17', '13:42:55.079846', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (384, '2019-11-17', '13:54:04.747386', 'Inserción de parametro: Context, all');
INSERT INTO program.log VALUES (385, '2019-11-17', '14:02:34.452248', 'Cambio de parametro: 19, Context de valor all a all');
INSERT INTO program.log VALUES (386, '2019-11-17', '14:03:50.506617', 'Eliminación de parametro: 19, 19');
INSERT INTO program.log VALUES (387, '2019-11-17', '15:04:17.012555', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p> a: <h3>Construcción del Código</h3>
<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p>');
INSERT INTO program.log VALUES (388, '2019-11-18', '14:33:50.654934', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (389, '2019-11-18', '16:28:01.527632', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (390, '2019-11-18', '17:14:00.001194', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (391, '2019-11-18', '17:22:03.683908', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (392, '2019-11-18', '17:36:43.23662', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (393, '2019-11-18', '17:49:05.93348', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (394, '2019-11-18', '18:03:08.761249', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (395, '2019-11-18', '18:03:53.674595', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (396, '2019-11-18', '18:07:09.261383', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (397, '2019-11-18', '18:20:27.797477', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (398, '2019-11-24', '08:44:29.747645', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (399, '2019-11-24', '08:51:23.206163', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (400, '2019-11-24', '10:00:41.961175', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (401, '2019-11-24', '10:29:48.991514', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (402, '2019-11-24', '11:32:26.565952', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (403, '2019-11-24', '11:33:53.618852', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (404, '2019-11-24', '11:45:34.985307', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (405, '2019-11-24', '11:53:06.667632', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (406, '2019-11-24', '11:59:00.934817', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (407, '2019-11-24', '12:38:37.216594', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (408, '2019-11-24', '19:16:58.673311', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (409, '2019-12-22', '10:07:01.941716', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (410, '2019-12-22', '10:08:49.524009', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (411, '2019-12-22', '10:09:39.352909', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (412, '2019-12-22', '15:00:08.137386', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (413, '2019-12-22', '15:02:39.771437', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (414, '2019-12-22', '15:02:59.529154', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (415, '2019-12-22', '18:42:30.962031', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (416, '2019-12-22', '18:43:20.134282', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (417, '2019-12-24', '11:22:20.680654', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (418, '2019-12-24', '11:22:22.378252', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (419, '2019-12-24', '11:22:55.921256', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (420, '2019-12-29', '09:09:28.839584', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (421, '2019-12-29', '09:23:33.555216', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (422, '2019-12-29', '10:09:53.340912', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (423, '2019-12-29', '10:27:32.997413', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (424, '2019-12-29', '10:35:58.891863', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (425, '2019-12-29', '11:05:18.999082', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (426, '2019-12-29', '11:15:08.338153', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (427, '2019-12-29', '11:43:19.051115', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (428, '2019-12-29', '12:01:00.950353', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (429, '2019-12-29', '12:07:32.289869', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (430, '2019-12-29', '12:19:02.773928', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (431, '2019-12-29', '12:28:27.070278', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (432, '2019-12-29', '13:17:59.19576', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (433, '2019-12-29', '13:20:41.509343', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (434, '2019-12-29', '13:23:16.277269', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (435, '2019-12-29', '13:28:31.058871', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (436, '2019-12-29', '13:30:46.578559', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (437, '2019-12-29', '13:33:02.317295', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (438, '2019-12-29', '13:44:33.903997', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (439, '2019-12-29', '13:50:07.004124', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (440, '2020-01-05', '12:27:32.509862', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (441, '2020-01-05', '12:32:22.036258', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (442, '2020-01-05', '12:53:19.344283', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (443, '2020-01-05', '12:54:21.994449', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (444, '2020-01-05', '12:56:59.971123', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (445, '2020-01-05', '13:00:58.690196', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (446, '2020-01-05', '13:03:16.812298', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (447, '2020-01-05', '13:05:19.904637', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (448, '2020-01-12', '12:24:58.325159', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (449, '2020-01-12', '12:28:05.192342', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (450, '2020-01-12', '12:29:25.540432', 'Cambio de parametro: 1, LOGO de valor  a /var/pruebaphp');
INSERT INTO program.log VALUES (451, '2020-01-12', '12:31:46.919375', 'Cambio de parametro: 1, LOGO de valor /var/pruebaphp a /var/prueba');
INSERT INTO program.log VALUES (452, '2020-01-12', '12:37:50.548289', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (453, '2020-01-12', '12:50:26.896972', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (454, '2020-01-12', '12:55:10.407057', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (455, '2020-01-12', '13:23:38.090524', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (456, '2020-01-12', '13:29:22.371037', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (457, '2020-01-12', '13:51:03.720878', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (458, '2020-01-12', '17:26:27.315266', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (459, '2020-01-12', '17:32:37.477666', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (460, '2020-01-12', '17:46:31.523176', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (461, '2020-01-12', '18:08:59.717802', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (462, '2020-01-12', '18:33:22.270403', 'Cambio de parametro: 1, LOGO de valor /var/prueba a /var/pruebaJava');
INSERT INTO program.log VALUES (463, '2020-01-19', '07:40:20.579981', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (464, '2020-01-19', '07:40:53.852284', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (465, '2020-01-19', '07:41:52.150964', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (466, '2020-01-19', '07:59:01.507288', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (467, '2020-01-19', '08:02:04.788921', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (468, '2020-01-19', '08:29:30.909782', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (469, '2020-01-19', '08:34:07.388301', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (470, '2020-01-19', '08:38:33.536091', 'Inserción de parametro: prueba, Prueba Java');
INSERT INTO program.log VALUES (471, '2020-01-19', '08:42:42.33634', 'Inserción de articulo: Prueba, <h1>Prueba</h1>, <p>Prueba</p>');
INSERT INTO program.log VALUES (472, '2020-01-19', '08:52:02.212096', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (473, '2020-01-19', '09:33:40.876633', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (474, '2020-01-19', '09:34:41.579363', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (475, '2020-01-19', '09:36:16.616666', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (476, '2020-01-19', '09:38:21.660748', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (477, '2020-01-19', '09:46:47.131945', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (478, '2020-01-19', '10:03:01.887149', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (479, '2020-01-19', '10:25:01.540686', 'Inserción de Usuario: prueba, 0');
INSERT INTO program.log VALUES (480, '2020-01-19', '10:25:01.540686', 'User, insertar Usuario prueba, perfil: 0');
INSERT INTO program.log VALUES (481, '2020-01-19', '10:32:12.504074', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (482, '2020-01-19', '10:42:37.565603', 'Eliminación de parametro: 20, 20');
INSERT INTO program.log VALUES (483, '2020-01-23', '07:32:09.46888', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (484, '2020-02-06', '11:16:10.079298', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (485, '2020-02-06', '14:12:31.273638', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (486, '2020-02-06', '14:26:25.551406', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (487, '2020-02-06', '14:27:14.206852', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (488, '2020-02-06', '14:27:56.54714', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (489, '2020-02-06', '14:29:24.950595', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (490, '2020-02-06', '14:31:44.276541', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (491, '2020-02-06', '14:32:43.520124', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (492, '2020-02-06', '14:34:53.102094', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (493, '2020-02-06', '14:50:23.775051', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (494, '2020-02-06', '14:51:16.558231', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (495, '2020-02-06', '14:52:01.093683', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (496, '2020-02-06', '14:52:13.113762', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (497, '2020-02-06', '14:55:44.055748', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (498, '2020-02-06', '14:58:04.746681', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (499, '2020-02-06', '14:59:55.238666', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (500, '2020-02-06', '15:25:30.143368', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (501, '2020-02-06', '15:27:20.009873', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (502, '2020-02-06', '15:28:49.457466', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (503, '2020-02-06', '15:29:24.778224', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (504, '2020-02-06', '15:29:44.087692', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (505, '2020-02-06', '17:08:01.211308', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (506, '2020-02-07', '08:27:49.846779', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (507, '2020-02-07', '08:38:25.052939', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (508, '2020-02-07', '09:16:33.609774', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (509, '2020-02-07', '09:46:39.649713', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (510, '2020-02-07', '09:47:53.077487', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (511, '2020-02-07', '11:08:48.09258', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (512, '2020-02-07', '11:26:11.347111', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>Se trata de simplificar el código para descargar peso sobre el navegador y lograr una mayor velocidad de ejecución</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (513, '2020-02-07', '12:08:22.086239', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (514, '2020-02-07', '12:09:05.751941', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (515, '2020-02-07', '12:09:22.703103', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (516, '2020-02-07', '12:10:11.853959', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (517, '2020-02-07', '12:13:35.907766', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (518, '2020-02-07', '14:46:07.412912', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (519, '2020-02-07', '14:47:01.430061', 'Inserción de parametro: prueba_3, prueba 3');
INSERT INTO program.log VALUES (520, '2020-02-07', '14:55:17.344934', 'Inserción de parametro: prueba_4, prueba 4');
INSERT INTO program.log VALUES (521, '2020-02-07', '15:00:05.766911', 'Eliminación de parametro: 21, 21');
INSERT INTO program.log VALUES (522, '2020-02-07', '15:06:00.914888', 'Eliminación de parametro: 22, 22');
INSERT INTO program.log VALUES (523, '2020-02-07', '15:26:20.33566', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (524, '2020-02-07', '15:31:04.977339', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (525, '2020-02-07', '15:32:36.478909', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (526, '2020-02-07', '15:33:02.720483', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (527, '2020-02-07', '15:34:04.441954', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (528, '2020-02-07', '15:36:17.596287', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (529, '2020-02-08', '09:24:49.632685', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (530, '2020-02-08', '09:25:42.075014', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (531, '2020-02-08', '09:32:54.668208', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (532, '2020-02-08', '09:43:23.110895', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (533, '2020-02-08', '09:43:23.120722', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (534, '2020-02-08', '10:09:43.020299', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (535, '2020-02-08', '10:10:47.072164', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (536, '2020-02-08', '10:26:10.007522', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (537, '2020-02-08', '10:34:06.4833', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (538, '2020-02-08', '10:35:40.843982', 'Acceso al sistema usuario undefined');
INSERT INTO program.log VALUES (539, '2020-02-08', '10:36:04.622378', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (540, '2020-02-09', '08:17:30.32148', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (541, '2020-02-09', '08:36:10.433407', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (542, '2020-02-09', '08:38:38.038344', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (543, '2020-02-16', '09:08:25.158365', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (544, '2020-02-16', '09:16:44.607697', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (545, '2020-02-16', '09:20:17.274551', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (546, '2020-02-16', '09:23:11.507769', 'Acceso al sistema usuario ');
INSERT INTO program.log VALUES (547, '2020-02-16', '09:30:14.856394', 'Acceso al sistema usuario ');
INSERT INTO program.log VALUES (548, '2020-02-16', '09:33:19.260973', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (549, '2020-02-16', '09:41:18.362821', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (550, '2020-02-16', '09:47:05.726543', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (551, '2020-02-16', '09:47:49.129702', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (552, '2020-02-16', '09:49:12.477972', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (553, '2020-02-16', '09:50:36.294231', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (554, '2020-02-16', '14:02:29.199159', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (555, '2020-02-16', '14:17:53.29672', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (556, '2020-02-16', '18:34:58.95497', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (557, '2020-02-16', '18:36:51.320756', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (558, '2020-02-16', '18:37:25.588602', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (559, '2020-02-16', '18:38:06.843222', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (560, '2020-02-16', '18:39:28.452796', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (561, '2020-02-16', '18:40:20.948714', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (562, '2020-02-16', '18:46:06.221971', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (563, '2020-02-16', '18:46:47.724802', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (564, '2020-02-16', '18:47:20.088113', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (565, '2020-02-16', '18:48:53.995197', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (566, '2020-02-16', '18:50:39.974041', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (567, '2020-02-16', '18:52:43.788151', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (568, '2020-02-16', '18:53:47.047734', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (569, '2020-02-16', '19:03:34.638551', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (570, '2020-02-16', '19:05:42.311924', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (571, '2020-02-16', '19:06:10.455997', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (572, '2020-02-16', '19:07:52.969079', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (573, '2020-02-16', '19:09:05.547557', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (574, '2020-02-16', '19:10:19.178842', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (575, '2020-02-16', '19:13:21.838056', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (576, '2020-02-16', '19:13:50.617386', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (577, '2020-03-27', '10:32:56.969374', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (578, '2020-03-27', '10:53:11.13048', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (579, '2020-03-27', '11:25:33.713188', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (580, '2020-04-01', '14:01:12.941399', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (581, '2020-04-01', '14:02:04.839015', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (582, '2020-04-01', '14:03:30.384659', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (583, '2020-04-01', '14:05:59.702647', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (584, '2020-04-01', '15:04:59.525378', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (585, '2020-04-01', '15:09:49.753539', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (586, '2020-04-01', '15:14:40.476156', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (587, '2020-04-01', '15:42:45.012137', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (588, '2020-04-01', '15:43:29.608389', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (589, '2020-04-01', '15:59:33.829848', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (590, '2020-04-01', '16:12:42.840926', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (591, '2020-04-01', '16:15:59.76045', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (592, '2020-04-01', '16:22:32.206405', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (593, '2020-04-01', '16:50:59.052591', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (594, '2020-04-01', '16:52:54.456572', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (595, '2020-04-02', '09:05:37.131575', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (596, '2020-04-02', '10:02:01.760932', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (597, '2020-04-02', '10:05:32.571669', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (598, '2020-04-02', '18:11:49.48154', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (599, '2020-04-02', '18:12:49.13926', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (600, '2020-04-02', '18:14:18.848048', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (601, '2020-04-02', '18:15:23.216207', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (602, '2020-04-02', '18:22:33.577977', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (603, '2020-04-02', '18:23:40.752857', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (604, '2020-04-02', '18:29:49.85689', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (605, '2020-04-02', '18:30:21.801798', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (606, '2020-04-02', '18:31:45.913854', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (607, '2020-04-05', '17:38:55.407926', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (608, '2020-04-05', '17:42:35.604928', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (609, '2020-04-05', '17:44:34.672582', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (610, '2020-04-05', '17:46:55.60132', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (611, '2020-04-05', '17:56:42.003362', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (612, '2020-04-05', '17:58:24.814519', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (613, '2020-04-05', '17:58:57.933383', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (614, '2020-04-05', '18:01:53.298887', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (615, '2020-04-05', '18:11:36.804794', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (616, '2020-04-05', '18:12:49.621756', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (617, '2020-04-05', '18:15:10.557614', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (618, '2020-04-05', '18:16:31.903694', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (619, '2020-04-05', '18:19:41.176282', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (620, '2020-04-05', '18:22:04.240874', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (621, '2020-04-05', '18:22:43.429917', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (622, '2020-04-05', '18:23:01.52164', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (623, '2020-04-05', '18:23:25.971495', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (624, '2020-04-05', '18:25:26.783525', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (625, '2020-04-05', '18:26:01.264095', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (626, '2020-04-05', '18:27:02.506447', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (627, '2020-04-05', '18:27:41.558172', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (628, '2020-04-05', '18:28:02.408822', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (629, '2020-04-05', '18:28:27.538887', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (630, '2020-04-05', '18:28:56.686165', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (631, '2020-04-05', '18:29:41.592637', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (632, '2020-04-05', '18:32:05.773618', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (633, '2020-04-05', '18:33:09.281142', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (634, '2020-04-05', '18:33:57.694754', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (635, '2020-04-05', '18:34:57.510065', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (636, '2020-04-08', '08:28:45.079674', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (637, '2020-04-08', '08:50:28.551286', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (638, '2020-04-08', '09:20:18.964003', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (639, '2020-04-08', '09:22:18.058365', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (640, '2020-04-08', '09:26:57.959752', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (641, '2020-04-08', '09:29:25.512161', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (642, '2020-04-08', '10:40:53.375192', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (643, '2020-04-08', '14:38:07.559846', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (644, '2020-04-08', '14:39:32.659654', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (645, '2020-04-08', '14:41:53.544758', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (646, '2020-04-08', '14:43:57.393093', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (647, '2020-04-08', '15:08:19.498972', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (648, '2020-04-08', '16:17:02.405965', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (649, '2020-04-08', '16:19:51.805139', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (650, '2020-04-08', '16:41:27.829468', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (651, '2020-04-08', '16:43:21.512258', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (652, '2020-04-08', '16:44:02.966336', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (653, '2020-04-08', '16:44:58.629627', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (654, '2020-04-10', '15:16:16.040804', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (655, '2020-04-10', '15:18:07.32176', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (656, '2020-04-10', '15:19:03.566912', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (657, '2020-04-10', '15:23:02.975426', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (658, '2020-04-10', '15:29:44.933304', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (659, '2020-04-10', '15:32:00.244568', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (660, '2020-04-10', '15:33:55.985844', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (661, '2020-04-10', '15:36:56.207062', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (662, '2020-04-10', '15:38:21.780005', 'Acceso al sistema usuario adimin');
INSERT INTO program.log VALUES (663, '2020-04-10', '15:39:09.194469', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (664, '2020-04-10', '15:40:04.555355', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (665, '2020-04-10', '15:41:31.313252', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (666, '2020-04-10', '15:43:16.05078', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (667, '2020-04-10', '15:43:49.247227', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (668, '2020-04-10', '15:44:20.641105', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (669, '2020-04-10', '15:46:06.022679', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (670, '2020-04-10', '16:08:25.166057', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (671, '2020-04-12', '14:32:52.439823', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (672, '2020-04-12', '14:42:58.23612', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (673, '2020-04-12', '17:20:03.560689', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (674, '2020-04-12', '17:24:10.697331', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (675, '2020-04-12', '17:34:50.049249', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (676, '2020-04-12', '17:37:02.389335', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (677, '2020-04-12', '17:38:17.151713', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (678, '2020-04-12', '17:42:40.052867', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (679, '2020-04-12', '17:44:04.807982', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (680, '2020-04-12', '17:44:23.520458', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (681, '2020-04-12', '18:25:00.036129', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (682, '2020-04-12', '18:28:18.094135', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (683, '2020-04-12', '18:30:14.728735', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (684, '2020-04-13', '10:39:30.865503', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (685, '2020-04-13', '10:41:07.140812', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (686, '2020-04-13', '10:52:18.155693', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (687, '2020-04-13', '10:55:50.190684', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (688, '2020-04-13', '10:57:36.417789', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (689, '2020-04-13', '10:58:51.272715', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (690, '2020-04-13', '10:59:43.726202', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (691, '2020-04-13', '11:01:11.721691', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (692, '2020-04-13', '11:02:08.248972', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (693, '2020-04-13', '11:15:00.749062', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (694, '2020-04-24', '14:44:29.109976', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (695, '2020-04-24', '15:06:28.717885', 'Acceso al sistema usuario ');
INSERT INTO program.log VALUES (696, '2020-04-24', '15:06:55.197114', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (697, '2020-04-24', '15:22:59.048544', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (701, '2020-04-24', '15:27:43.559401', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (702, '2020-04-24', '21:33:45.282588', 'Acceso al sistema usuario ');
INSERT INTO program.log VALUES (703, '2020-06-07', '17:48:52.931301', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (704, '2020-06-07', '17:49:17.686605', 'Acceso al sistema usuario Admin');
INSERT INTO program.log VALUES (705, '2020-06-07', '17:49:34.502104', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (706, '2020-06-07', '18:06:02.230608', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El diseño del sistema, desde su concepción, es descentralizado, conformado por 3 áreas, un Front End, un Back End y la lógica de la base de datos.</p>
<h5>El FontEnd</h5>
<p>Esta conformado para ser utilizado en navegadores Web, compatible con Mozilla Firefox y Google Chrome, del mismo se desarrollaron 3 versiones:</p>
<ul>
	<li>HTML5 – JavaScript (Utilizables desde cualquier servidor Web como Apache 2.4, NGINX, etc)</li>
	<li>React (Utilizable desde Node.js v-12.X)</li>
	<li>Angular (Utilizable desde Node.js v-12.X)</li>
</ul>
<p>Consiste en:</p>
<ul>
	<li>Desplegador de paginas de bienvenida, los cuales se pueden editar dentro del mismo sistema, agregar, eliminar y modificar (ABM)</li>
	<li>Acceso al sistema, con usuario y clave.</li>
	<li>Sistema de menús: está área del sistema, se modifica internamente en la base de datos, y la misma es dependiente de la instalación, esta pensado para el manejo de perfiles, lo que permite que se asignen accesos a cada usuario de forma individual</li>
	<li>ABM (Agregar, Borrar, Modificar) de datos: Para las tablas de Parámetros, Vistas y Usuarios, lo que incluye el listado de los mismos.</li>
</ul>
<h5>Back End</h5>
<p>Esta compuesto de un servicio RestFul, que comprende desde el acceso de los usuarios, hasta las actividades de los ABM de las tablas.</p>
<p>Están desarrollados en 3 idiomas: </p>
<ul>
	<li>PHP 7.2 (Para servidores Apache 2.4 ó NGINX)</li>
	<li>Java 8 (Para servidores Tomcat, Wildcat, etc.)</li>
	<li>Node.js con Express (Para servidores Node.js)</li>
</ul>
<p>El mismo tiene el sistema de verificación JWT (Json Web Token) para cada uno de las solicitudes, respondiendo a través de AJAX.</p>
<h5>Base de datos</h5>
<p>Esta desarrollado en PostgreSQL, pero se puede migrar a cualquier base de datos SQL, esta basado en funciones, los cuales son accesibles con un usuario con capacidades limitadas, impidiendo junto con el hecho que no se utilizan comandos SQL directos la inyección de código.</p>
<p>Para la migración de las bases de datos, se requiere que se nombren con las mismas funciones, recibiendo y enviando los mismos datos.</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (707, '2020-06-07', '18:08:46.387284', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (708, '2020-06-07', '18:10:29.620078', 'Inserción de articulo: Codigo, <h1>Código Desarrollado</h1>, <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (709, '2020-06-07', '18:10:59.107287', 'Edición de articulo: 2 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>Código Desarrollado</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (710, '2020-06-07', '18:13:33.82824', 'Edición de articulo: 12 de: Codigo a: Codigo; <h1>Código Desarrollado</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (711, '2020-06-07', '18:14:49.520989', 'Edición de articulo: 12 de: Codigo a: Codigo; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (712, '2020-06-07', '18:18:55.673983', 'Edición de articulo: 12 de: Codigo a: Codigo; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (713, '2020-06-07', '18:19:28.520868', 'Edición de articulo: 12 de: Codigo a: Codigo1; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (714, '2020-06-07', '18:23:51.089921', 'Edición de articulo: 12 de: Codigo1 a: IA; <h1>La inteligencia artificial, es parte de la nueva era</h1> a: <h1>La inteligencia artificial, es parte de la nueva era</h1>, de: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p> a: <h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (715, '2020-06-07', '18:38:50.813301', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (716, '2020-06-07', '18:41:46.532063', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (717, '2020-06-07', '19:20:41.245585', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (718, '2020-06-07', '19:24:29.812987', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (719, '2020-06-07', '19:26:00.433602', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (720, '2020-06-07', '19:35:59.189736', 'Edición de articulo: 1 de: Inicio a: Codigo; <h1>Desarrollo de Menu Program</h1> a: <h1>Código Desarrollado</h1>, de: <div class="row">
    <div class="col-3">
        <div class="card" style="width: 16rem;">
            <div class="card-body">
                <h5 class="card-title"> Base de Datos</h5>
                <p class="card-text">La estructura de la base de datos, esta pensada para contener todo lo referente al sistema (paginas de presentación, gestión de acceso y usuarios, perfiles de usuarios y variables de entorno)  y aparte los datos de la empresa, creando un entorno más seguro para dicha información.</p>
                <p class="card-text">El contenido de los datos del sistema, consiste en usuarios, claves, perfiles, paginas externas, estructuras de menú, parámetros y los logs de actividad.</p>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="card" style="width: 16rem;">
            <div class="card-body">
                <h5 class="card-title">Back End</h5>
                <p class="card-text">Creado para ser RestFull, fue desarrollado en varios idiomas para adaptar a diversos entornos: PHP, Node.js, Java (Spark).</p> 
                <p class="card-text">Respondiendo a través de Ajax, utilizando un servidor Apache 2.4, o NGINX configurados como proxys inversos, evitando por este medio el error CORS en los navegadores.</p>
                <p class="card-text">La verificación se ejecuta a través JWT (Json Web Token) el cual usa los valores del nombre del usuario y el perfil para validar que la conexión sea valida.</p>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="card" style="width: 16rem;">
            <div class="card-body">
                <h5 class="card-title">Front End</h5>
                <p class="card-text">Las interfaces Web fueron desarrolladas en HTML, React y Angular, a gusto del cliente, verificando la independencia de origen de datos.</p>
                <p class="card-text">Las ventajas de utilizar interfaces Web representan la independencia del sistema operativo, uso en cualquier plataforma, y la disponibilidad desde cualquier punto que disponga de Internet.</p>
                <p class="card-text">Además de la disponibilidad y flexibilidad, ofrece la ventaja que al ser separados, podemos desplegar de diversas formas el sistema.</p>
            </div>
        </div>
    </div>
    <div class="col-3">
        <div class="card" style="width: 16rem;">
            <div class="card-body">
                <h5 class="card-title">Servidores</h5>
                <p class="card-text">El despliegue de la aplicación, se puede hacer desde cualquier servidor Web (Apache, NGINX) para el Front End HTML-JavaScript, y para el Back End PHP; Node.js con el Front End React y el Back End Express, Tomcat, para el Back End Java.</p>
                <p class="card-text">Lo que implica la disponibilidad en Windows, Linux y MacOS, tanto para el servidor como para la interfaz frontal de datos.</p>
                <p class="card-text">Para evitar el CORS, se utiliza como interfaz unica Apache o NGINX configurados como proxys inversos.</p>
            </div>
        </div>
    </div>
</div> a: <h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>');
INSERT INTO program.log VALUES (721, '2020-09-27', '16:49:52.143032', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (722, '2020-09-27', '17:01:48.921583', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (723, '2020-09-27', '19:59:15.91052', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (724, '2020-10-04', '09:16:28.785899', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (725, '2020-10-04', '09:18:15.384868', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (726, '2020-10-04', '09:31:30.036498', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (727, '2020-10-04', '09:32:47.209233', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (728, '2020-10-04', '09:33:17.035009', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (729, '2020-10-04', '09:35:35.841607', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (730, '2020-10-04', '18:51:28.889915', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (731, '2020-10-04', '18:58:12.402503', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (732, '2020-10-04', '19:00:23.737404', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (733, '2020-10-04', '19:03:08.242579', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (734, '2020-10-04', '19:07:21.383089', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (735, '2020-10-04', '19:16:49.006023', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (736, '2020-10-04', '19:21:30.812233', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (737, '2020-10-04', '19:24:27.047617', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (738, '2020-10-04', '19:25:48.066103', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (739, '2020-10-04', '19:35:54.571599', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (740, '2020-10-11', '12:14:25.50841', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (741, '2020-10-11', '12:19:20.773419', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (742, '2020-10-11', '12:23:26.292711', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (743, '2020-10-11', '12:26:19.502756', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (744, '2020-11-01', '11:38:01.303015', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (745, '2020-11-01', '11:38:46.139898', 'Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p> a: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe src="rastrahm.com" ></iframe>');
INSERT INTO program.log VALUES (746, '2020-11-01', '11:39:03.385196', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (747, '2020-11-01', '11:39:38.132374', 'Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe src="rastrahm.com" ></iframe> a: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe src="http://rastrahm.com" ></iframe>');
INSERT INTO program.log VALUES (748, '2020-11-01', '11:40:33.22234', 'Edición de articulo: 1 de: Inicio a: Inicio; <h1>Inicio</h1> a: <h1>Inicio</h1>, de: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe src="http://rastrahm.com" ></iframe> a: <h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe style=""width:100%;"   src="http://rastrahm.com" ></iframe>');
INSERT INTO program.log VALUES (749, '2020-11-22', '14:33:19.607155', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (750, '2020-11-22', '14:36:39.91402', 'Acceso al sistema usuario admin');
INSERT INTO program.log VALUES (751, '2020-11-22', '14:36:53.320506', 'Acceso al sistema usuario admin');


--
-- TOC entry 3174 (class 0 OID 16513)
-- Dependencies: 210
-- Data for Name: menu; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.menu VALUES (1, 0, 'Archivos', 'Acceso a las opcines de archivos');
INSERT INTO program.menu VALUES (4, 0, 'Ayuda', 'Menú de ayuda');
INSERT INTO program.menu VALUES (3, 0, 'Preferencias', 'Opciones del sistema');


--
-- TOC entry 3178 (class 0 OID 16534)
-- Dependencies: 214
-- Data for Name: menu_options; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.menu_options VALUES (19, 3, 0, 'Articulos ', 'Edición de los articulos externos del sistema', NULL, true, 'objects', 'fnc_previews', '{''str_link'',''txt_title'',''txt_article''}', 'previews');
INSERT INTO program.menu_options VALUES (17, 3, 0, 'Usuarios', 'Creación y modificación de usuarios', NULL, true, 'objects', 'fnc_users', '{''str_name'',''bol_active'',''int_pro''}', 'users');
INSERT INTO program.menu_options VALUES (9, 1, 0, 'Salir', 'Termina la aplicación', 'Alt + F4', true, NULL, NULL, NULL, 'exit');
INSERT INTO program.menu_options VALUES (20, 4, 0, 'Ayuda', 'Indice de ayuda', 'F1', true, NULL, NULL, NULL, 'help');
INSERT INTO program.menu_options VALUES (21, 4, 0, 'Acerca de', 'Acerca de la aplicacion', NULL, true, NULL, NULL, NULL, 'about');
INSERT INTO program.menu_options VALUES (16, 3, 0, 'Opciones', 'Opciones del sistema', NULL, true, 'objects', 'fnc_parameter', '{''str_name'',''txt_value'',''bol_visible''}', 'parameters');


--
-- TOC entry 3166 (class 0 OID 16420)
-- Dependencies: 202
-- Data for Name: parameter; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.parameter VALUES (2, 'APP_WIDTH', '600px', 1);
INSERT INTO program.parameter VALUES (3, 'APP_HEIGHT', '400px', 1);
INSERT INTO program.parameter VALUES (4, 'JWT_KEY', 'Sistema Sistema Sistema Sistema Sistema Sistema Sistema SistemaSistema Sistema Sistema Sistema v Sistema Sistema Sistema Sistema Sistema Sistema Sistema Sistema Sistema v Sistema Sistema v Sistema Sistema Sistema Sistema v Sistema Sistema Sistema Sistemav', 0);
INSERT INTO program.parameter VALUES (1, 'LOGO', '/var/pruebaJava', 1);


--
-- TOC entry 3182 (class 0 OID 16687)
-- Dependencies: 223
-- Data for Name: preview; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.preview VALUES (2, '<h1>Código Desarrollado</h1>', '<h3>Construcción del Código</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>', 'Codigo');
INSERT INTO program.preview VALUES (12, '<h1>La inteligencia artificial, es parte de la nueva era</h1>', '<h3>IA</h3>
<p>El código esta construido en varios Front Ends y varios Back Ends (PHP, Java, Node js) para respaldar el funcionamiento en los entornos más comunes, según los servidores deseados por el usuario</p>', 'IA');
INSERT INTO program.preview VALUES (1, '<h1>Inicio</h1>', '<h3>Una estructura flexible</h3>
<p>Estructura de base, que comprende estructura de los usuarios y menús de la aplicación.</p>
<iframe style=""width:100%;"   src="http://rastrahm.com" ></iframe>', 'Inicio');


--
-- TOC entry 3170 (class 0 OID 16487)
-- Dependencies: 206
-- Data for Name: profile; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.profile VALUES (0, 'Administrador');


--
-- TOC entry 3164 (class 0 OID 16411)
-- Dependencies: 200
-- Data for Name: user; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program."user" VALUES (1, 'admin', 1, 0);
INSERT INTO program."user" VALUES (2, 'rasg', 1, 0);
INSERT INTO program."user" VALUES (10, 'prueba', 1, 0);


--
-- TOC entry 3180 (class 0 OID 16569)
-- Dependencies: 216
-- Data for Name: user_pass; Type: TABLE DATA; Schema: program; Owner: rasg
--

INSERT INTO program.user_pass VALUES (4, 1, '2019-07-09 11:16:33.145685', '\x18eacccfb6793b3618baf3e320d53be02b93d113');
INSERT INTO program.user_pass VALUES (6, 1, '2019-07-09 11:21:53.904977', '\x18eacccfb6793b3618baf3e320d53be02b93d113');
INSERT INTO program.user_pass VALUES (7, 1, '2019-07-09 11:23:07.043242', '\x03684d3ef662a25b801055c5fa35f24e');
INSERT INTO program.user_pass VALUES (8, 1, '2019-07-09 11:46:43.116466', '\x4d8bec073f52bdea');
INSERT INTO program.user_pass VALUES (9, 1, '2019-07-09 13:58:31.626255', '\x4d8bec073f52bdea');


--
-- TOC entry 3254 (class 0 OID 0)
-- Dependencies: 225
-- Name: dictionary_dic_id_seq; Type: SEQUENCE SET; Schema: program; Owner: postgres
--

SELECT pg_catalog.setval('program.dictionary_dic_id_seq', 1, false);


--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 203
-- Name: log_log_id_seq; Type: SEQUENCE SET; Schema: program; Owner: postgres
--

SELECT pg_catalog.setval('program.log_log_id_seq', 751, true);


--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 208
-- Name: menu_men_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_men_id_seq', 1, false);


--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 212
-- Name: menu_options_men_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_men_id_seq', 3, true);


--
-- TOC entry 3258 (class 0 OID 0)
-- Dependencies: 211
-- Name: menu_options_mop_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_mop_id_seq', 1, false);


--
-- TOC entry 3259 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_options_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_options_pro_id_seq', 4, true);


--
-- TOC entry 3260 (class 0 OID 0)
-- Dependencies: 209
-- Name: menu_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.menu_pro_id_seq', 4, true);


--
-- TOC entry 3261 (class 0 OID 0)
-- Dependencies: 201
-- Name: parameter_par_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.parameter_par_id_seq', 22, true);


--
-- TOC entry 3262 (class 0 OID 0)
-- Dependencies: 224
-- Name: preview_pre_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.preview_pre_id_seq', 12, true);


--
-- TOC entry 3263 (class 0 OID 0)
-- Dependencies: 205
-- Name: profile_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.profile_pro_id_seq', 1, false);


--
-- TOC entry 3264 (class 0 OID 0)
-- Dependencies: 199
-- Name: user_id_usr_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_id_usr_seq', 10, true);


--
-- TOC entry 3265 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_pass_psw_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pass_psw_id_seq', 9, true);


--
-- TOC entry 3266 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_pass_usr_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pass_usr_id_seq', 1, true);


--
-- TOC entry 3267 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_pro_id_seq; Type: SEQUENCE SET; Schema: program; Owner: rasg
--

SELECT pg_catalog.setval('program.user_pro_id_seq', 1, false);


--
-- TOC entry 3033 (class 2606 OID 16756)
-- Name: dictionary id_dictionary_pk; Type: CONSTRAINT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.dictionary
    ADD CONSTRAINT id_dictionary_pk PRIMARY KEY (dic_id);


--
-- TOC entry 3021 (class 2606 OID 16436)
-- Name: log ind_log; Type: CONSTRAINT; Schema: program; Owner: postgres
--

ALTER TABLE ONLY program.log
    ADD CONSTRAINT ind_log PRIMARY KEY (log_id);


--
-- TOC entry 3025 (class 2606 OID 16522)
-- Name: menu ind_menu; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu
    ADD CONSTRAINT ind_menu PRIMARY KEY (men_id);


--
-- TOC entry 3027 (class 2606 OID 16544)
-- Name: menu_options ind_menu_options; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options PRIMARY KEY (mop_id);


--
-- TOC entry 3019 (class 2606 OID 16425)
-- Name: parameter ind_parameter; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.parameter
    ADD CONSTRAINT ind_parameter PRIMARY KEY (par_id);


--
-- TOC entry 3031 (class 2606 OID 16700)
-- Name: preview ind_preview; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.preview
    ADD CONSTRAINT ind_preview PRIMARY KEY (pre_id);


--
-- TOC entry 3023 (class 2606 OID 16492)
-- Name: profile ind_profile; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.profile
    ADD CONSTRAINT ind_profile PRIMARY KEY (pro_id);


--
-- TOC entry 3029 (class 2606 OID 16574)
-- Name: user_pass ind_user_pass; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass
    ADD CONSTRAINT ind_user_pass PRIMARY KEY (psw_id);


--
-- TOC entry 3017 (class 2606 OID 16417)
-- Name: user ind_usuario; Type: CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user"
    ADD CONSTRAINT ind_usuario PRIMARY KEY (usr_id);


--
-- TOC entry 3161 (class 2618 OID 16638)
-- Name: user user_insert; Type: RULE; Schema: program; Owner: rasg
--

CREATE RULE user_insert AS
    ON INSERT TO program."user" DO  INSERT INTO program.log (log_date, log_time, log_event)
  VALUES (CURRENT_DATE, CURRENT_TIME, concat('User, insertar Usuario ', new.usr_name, ', perfil: ', new.pro_id));


--
-- TOC entry 3162 (class 2618 OID 16639)
-- Name: user user_update; Type: RULE; Schema: program; Owner: rasg
--

CREATE RULE user_update AS
    ON UPDATE TO program."user" DO  INSERT INTO program.log (log_date, log_time, log_event)
  VALUES (CURRENT_DATE, CURRENT_TIME, concat('User, actualización Usuario: ', new.usr_name, ', Activo: ', new.usr_active, ', Perfil: ', new.pro_id));


--
-- TOC entry 3039 (class 2620 OID 16600)
-- Name: user_pass encrypt_pass; Type: TRIGGER; Schema: program; Owner: rasg
--

CREATE TRIGGER encrypt_pass BEFORE INSERT OR UPDATE ON program.user_pass FOR EACH ROW EXECUTE PROCEDURE public.encryp_password();


--
-- TOC entry 3036 (class 2606 OID 16545)
-- Name: menu_options ind_menu_options_menu; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options_menu FOREIGN KEY (men_id) REFERENCES program.menu(men_id);


--
-- TOC entry 3037 (class 2606 OID 16550)
-- Name: menu_options ind_menu_options_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu_options
    ADD CONSTRAINT ind_menu_options_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3035 (class 2606 OID 16523)
-- Name: menu ind_menu_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.menu
    ADD CONSTRAINT ind_menu_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3038 (class 2606 OID 16590)
-- Name: user_pass ind_user_pass-user; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program.user_pass
    ADD CONSTRAINT "ind_user_pass-user" FOREIGN KEY (usr_id) REFERENCES program."user"(usr_id);


--
-- TOC entry 3034 (class 2606 OID 16503)
-- Name: user ind_user_profile; Type: FK CONSTRAINT; Schema: program; Owner: rasg
--

ALTER TABLE ONLY program."user"
    ADD CONSTRAINT ind_user_profile FOREIGN KEY (pro_id) REFERENCES program.profile(pro_id);


--
-- TOC entry 3191 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA program; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA program TO user_menu_program;


--
-- TOC entry 3193 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA public TO user_menu_program;


--
-- TOC entry 3194 (class 0 OID 0)
-- Dependencies: 300
-- Name: FUNCTION fnc_parameter_put(_id bigint, _name character varying, _value text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) TO program;
GRANT ALL ON FUNCTION public.fnc_parameter_put(_id bigint, _name character varying, _value text) TO user_menu_program;


--
-- TOC entry 3195 (class 0 OID 0)
-- Dependencies: 308
-- Name: FUNCTION fnc_previews_post(_link character varying, _title text, _article text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) TO program;
GRANT ALL ON FUNCTION public.fnc_previews_post(_link character varying, _title text, _article text) TO user_menu_program;


--
-- TOC entry 3196 (class 0 OID 0)
-- Dependencies: 309
-- Name: FUNCTION fnc_previews_put(_id bigint, _link character varying, _title text, _article text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) TO program;
GRANT ALL ON FUNCTION public.fnc_previews_put(_id bigint, _link character varying, _title text, _article text) TO user_menu_program;


--
-- TOC entry 3197 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE dictionary; Type: ACL; Schema: program; Owner: postgres
--

GRANT SELECT ON TABLE program.dictionary TO program;
GRANT SELECT ON TABLE program.dictionary TO user_menu_program;


--
-- TOC entry 3199 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE log; Type: ACL; Schema: program; Owner: postgres
--

GRANT SELECT,INSERT,UPDATE ON TABLE program.log TO user_menu_program;


--
-- TOC entry 3201 (class 0 OID 0)
-- Dependencies: 203
-- Name: SEQUENCE log_log_id_seq; Type: ACL; Schema: program; Owner: postgres
--

GRANT ALL ON SEQUENCE program.log_log_id_seq TO user_menu_program;


--
-- TOC entry 3206 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE menu; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT ON TABLE program.menu TO user_menu_program;


--
-- TOC entry 3217 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE menu_options; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT ON TABLE program.menu_options TO user_menu_program;


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE parameter; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE program.parameter TO user_menu_program;


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 201
-- Name: SEQUENCE parameter_par_id_seq; Type: ACL; Schema: program; Owner: rasg
--

GRANT ALL ON SEQUENCE program.parameter_par_id_seq TO user_menu_program;


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE preview; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE program.preview TO user_menu_program;


--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 224
-- Name: SEQUENCE preview_pre_id_seq; Type: ACL; Schema: program; Owner: rasg
--

GRANT ALL ON SEQUENCE program.preview_pre_id_seq TO user_menu_program;


--
-- TOC entry 3236 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE profile; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE program.profile TO user_menu_program;


--
-- TOC entry 3238 (class 0 OID 0)
-- Dependencies: 205
-- Name: SEQUENCE profile_pro_id_seq; Type: ACL; Schema: program; Owner: rasg
--

GRANT ALL ON SEQUENCE program.profile_pro_id_seq TO user_menu_program;


--
-- TOC entry 3243 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE "user"; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT,INSERT,UPDATE ON TABLE program."user" TO user_menu_program;


--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 199
-- Name: SEQUENCE user_id_usr_seq; Type: ACL; Schema: program; Owner: rasg
--

GRANT ALL ON SEQUENCE program.user_id_usr_seq TO user_menu_program;


--
-- TOC entry 3250 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE user_pass; Type: ACL; Schema: program; Owner: rasg
--

GRANT SELECT,INSERT,UPDATE ON TABLE program.user_pass TO user_menu_program;


-- Completed on 2020-12-25 11:04:21 -03

--
-- PostgreSQL database dump complete
--

